#HW_L8_T2_CustomHashMapImpl

Реализовать класс CustomHashMapImpl<K, V>, который представляет отображение на основе хеш-таблицы.

Класс CustomHashMapImpl реализует интерфейс CustomHashMap<K, V>
Класс CustomHashMapImpl может хранить объекты любого типа

##Конструкторы

CustomHashMapImpl();

Ссылка на CustomHashMap<K, V>: #ref

#Критерии приемки

1. Создать ветку feature/CustomHashMap
2. Добавить интерфейс CustomHashMap в ветку, сделать PUSH в удаленный репозиторий

3. Создать ветку feature/CustomHashMapImpl от ветки feature/CustomHashMap

4. Написать реализацию класса CustomHashMapImpl

5. Предоставить на проверку Pull Request из ветки feature/CustomHashMapImpl в ветку feature/CustomHashMap

6. Каждый публичный метод класса CustomHashMapImpl должен быть покрыт unit тестом

7. !!! Вносить правки в интерфейс CustomHashMap<K, V> нельзя

