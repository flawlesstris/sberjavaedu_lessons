package ru.edu;

import java.util.*;

/**
 * Hello world!
 */
public class Application {

    public static void main(String[] args) {
//        sortedSetExample();

//        treeSetExample();

//        hashSetExample();
        linkedHashSetExample();
    }

    private static void linkedHashSetExample() {

        Set<String> set = new LinkedHashSet<>();

        set.add("Anton");
        set.add("Anton");
        set.add("Denis");
        set.add("Vasilisa");
        set.add("Nick");

        System.out.println(set);

        set.remove("Denis");

        System.out.println(set);

//        System.out.println("set.contains('Denis'): " + set.contains("Denis"));
//        System.out.println("set.contains('Vasya'): " + set.contains("Vasya"));
    }

    private static void hashSetExample() {

        Set<String> set = new HashSet<>();

        set.add("Anton");
        set.add("Anton");
        set.add("Denis");
        set.add("Vasilisa");
        set.add("Nick");

        System.out.println(set);

        System.out.println("set.contains('Denis'): " + set.contains("Denis"));
        System.out.println("set.contains('Vasya'): " + set.contains("Vasya"));
    }

    private static void treeSetExample() {

        NavigableSet<String> set = new TreeSet<>((lhs, rhs) -> lhs.toLowerCase().compareTo(rhs.toLowerCase()));

        set.add("Anton");
        set.add("Denis");
        set.add("Vasilisa");
        set.add("Denis");
        set.add("Anna");
        set.add("anna");

        System.out.println(set);

        Set<String> aNames = set.subSet("A", "b");
        System.out.println(aNames);

    }

    private static void sortedSetExample() {

        NavigableSet<Integer> sortedSet = new TreeSet<>();

        sortedSet.add(10);
        sortedSet.add(5);
        sortedSet.add(101);
        sortedSet.add(5);
        sortedSet.add(45);

        System.out.println(sortedSet);

        System.out.println("sortedSet.first(): " + sortedSet.first());
        System.out.println("sortedSet.last(): " + sortedSet.last());

//        System.out.println("sortedSet.lower(10): " + sortedSet.lower(10));
//        System.out.println("sortedSet.floor(10): " + sortedSet.floor(10));
//
//        System.out.println("sortedSet.higher(10): " + sortedSet.higher(10));
//        System.out.println("sortedSet.ceiling(10): " + sortedSet.ceiling(10));
//
//        SortedSet<Integer> subset = sortedSet.subSet(10, true, 45, true);
//        System.out.println(subset);
//
//        SortedSet<Integer> headSet = sortedSet.headSet(45, true);
//        System.out.println(headSet);
//
//        SortedSet<Integer> tailSet = sortedSet.headSet(45, true);
//        System.out.println(tailSet);

//        System.out.println("decs: " + sortedSet.descendingSet());
//
//        Iterator<Integer> it = sortedSet.descendingIterator();
//
//        while (it.hasNext()) {
//            System.out.println("Elem: " + it.next());
//        }
    }
}
