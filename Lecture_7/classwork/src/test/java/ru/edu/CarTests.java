package ru.edu;

import org.junit.Assert;
import org.junit.Test;

import javax.swing.plaf.SpinnerUI;
import java.util.ArrayList;
import java.util.List;

public class CarTests {

    @Test
    public void notEqualsTest() {

        Car bmwCar = new Car("BWM", "white", 1992);
        Car ladaCar = new Car("Lada", "yellow", 2000);

        Assert.assertNotEquals(bmwCar, ladaCar);
    }

    @Test
    public void equalsTest() {

        Car firstCar = new Car("BWM", "white", 1992);
        Car secondCar = new Car("BWM", "white", 2000);

        Assert.assertEquals(secondCar, firstCar);
    }

    @Test
    public void comparableTest() {

        Car firstCar = new Car("BWM", "white", 1992);
        Car secondCar = new Car("BWM", "white", 2000);
        Car thirdCar = new Car("Mazda", "red", 1900);

        List<Car> carList = new ArrayList<>();

        carList.add(firstCar);
        carList.add(secondCar);
        carList.add(thirdCar);

        System.out.println(carList);

        carList.sort(Car::compareTo);

        System.out.println(carList);
    }

    @Test
    public void comparatorTest() {

        MyCarYearComparator comparator = new MyCarYearComparator();

        Car firstCar = new Car("BWM", "white", 1992);
        Car secondCar = new Car("BWM", "white", 2000);
        Car thirdCar = new Car("Mazda", "red", 1900);

        List<Car> carList = new ArrayList<>();

        carList.add(firstCar);
        carList.add(secondCar);
        carList.add(thirdCar);

        System.out.println(carList);

        carList.sort(comparator);

        System.out.println(carList);
    }
}
