package ru.edu;

import org.junit.Assert;
import org.junit.Test;

public class CustomHashSetImplTests {

    @Test
    public void hashSetTest() {

        CustomHashSetImpl<String> set = new CustomHashSetImpl<>();

        Assert.assertEquals(0, set.getSize());

        set.add("Anton");
        set.add("Anton");
        set.add("Denis");
        set.add("Vasilisa");
        set.add("Nick");

        Assert.assertEquals(4, set.getSize());
        System.out.println(set);
    }
}
