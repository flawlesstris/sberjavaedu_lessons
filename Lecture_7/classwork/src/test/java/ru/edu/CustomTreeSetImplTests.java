package ru.edu;

import org.junit.Assert;
import org.junit.Test;

public class CustomTreeSetImplTests {

    @Test
    public void treeSetTests() {

        CustomTreeSetImpl<Integer> set = new CustomTreeSetImpl<>(Integer::compareTo);

        Assert.assertEquals(0, set.getSize());

        set.add(10);
        set.add(10);
        set.add(20);
        set.add(100);
        set.add(0);

        Assert.assertEquals(4, set.getSize());

        Assert.assertTrue(set.contains(10));
        Assert.assertTrue(set.contains(100));
        Assert.assertTrue(set.contains(0));
        Assert.assertTrue(set.contains(20));

        Assert.assertFalse(set.contains(15));

        System.out.println(set);
    }
}
