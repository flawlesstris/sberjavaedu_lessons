#HW_L7_T3_CustomTreeSetImpl

Реализовать класс CustomTreeSetImpl<T>, который представляет множество на основе бинарного дерева поиска.

Класс CustomTreeSetImpl реализует интерфейс CustomTreeSet<T>
Класс CustomTreeSetImpl может хранить объекты любого типа

##Конструкторы

CustomTreeSetImpl(Comparator<T> comparator);

Ссылка на CustomTreeSet<T>: #ref

#Критерии приемки

1. Создать ветку feature/CustomTreeSet
2. Добавить интерфейс CustomTreeSet в ветку, сделать PUSH в удаленный репозиторий

3. Создать ветку feature/CustomTreeSetImpl от ветки feature/CustomTreeSet

4. Написать реализацию класса CustomTreeSetImpl

5. Предоставить на проверку Pull Request из ветки feature/CustomTreeSetImpl в ветку feature/CustomTreeSet

6. Каждый публичный метод класса CustomTreeSetImpl должен быть покрыт unit тестом

7. !!! Вносить правки в интерфейс CustomTreeSet<T> нельзя

