package ru.edu;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PersonComparingTests {

    @Test
    public void comparingTest() {

        List<Person> list = new ArrayList<>(Arrays.asList(
            new Person("DeNis", "Omsk", 21),
            new Person("AnTon", "Omsk", 19),
            new Person("Anton", "MosCow", 23),
            new Person("AnTon", "OmSk", 19),
            new Person("Anton", "MoscoW", 21),
            new Person("AntoN", "Omsk", 19)
        ));

        list.sort(Person::compareTo);
        System.out.println(list);

        Assert.assertEquals(new Person("Anton", "MosCow", 23), list.get(0));
        Assert.assertEquals(new Person("Anton", "MoscoW", 21), list.get(1));

        Assert.assertEquals(new Person("AnTon", "Omsk", 19), list.get(2));
        Assert.assertEquals(new Person("AnTon", "OmSk", 19), list.get(3));
        Assert.assertEquals(new Person("AntoN", "Omsk", 19), list.get(4));
        Assert.assertEquals(new Person("DeNis", "Omsk", 21), list.get(5));
    }
}