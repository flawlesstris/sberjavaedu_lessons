package ru.edu;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;

public class CustomHashSetTests {

    private CustomHashSet<Integer> set = null; //new CustomHashSetImpl<>(16);

    @Test
    public void sizeTest() {

        Assert.assertEquals(0, set.size());
        Assert.assertTrue(set.isEmpty());

        for (int i = 1; i <= 10; ++i) {
            Assert.assertTrue(set.add(i));
            Assert.assertEquals(i, set.size());
        }

        for (int i = 10; i >= 1; --i) {
            Assert.assertFalse(set.add(i));
        }
        Assert.assertEquals(10, set.size());

        for (int i = 10; i >= 1; --i) {
            Assert.assertTrue(set.remove(i));
            Assert.assertEquals(i - 1, set.size());
        }

        Assert.assertTrue(set.isEmpty());
    }

    @Test
    public void addTest() {

        Assert.assertEquals(0, set.toArray().length);

        Assert.assertTrue(set.add(10));
        Assert.assertFalse(set.add(10));
        Assert.assertEquals(Arrays.asList(10), Arrays.asList(set.toArray()));

        Assert.assertTrue(set.add(5));
        Assert.assertEquals(Arrays.asList(5, 10), Arrays.asList(set.toArray()));

        Assert.assertTrue(set.add(15));
        Assert.assertFalse(set.add(15));
        Assert.assertEquals(Arrays.asList(15, 5, 10), Arrays.asList(set.toArray()));

        Assert.assertFalse(set.add(15));
        Assert.assertEquals(Arrays.asList(15, 5, 10), Arrays.asList(set.toArray()));

        Assert.assertTrue(set.add(7));
        Assert.assertEquals(Arrays.asList(15, 5, 7, 10), Arrays.asList(set.toArray()));

        Assert.assertTrue(set.add(1));
        Assert.assertEquals(Arrays.asList(15, 1, 5, 7, 10), Arrays.asList(set.toArray()));

        Assert.assertTrue(set.add(9));
        Assert.assertEquals(Arrays.asList(15, 1, 5, 7, 9, 10), Arrays.asList(set.toArray()));

        Assert.assertTrue(set.add(20));
        Assert.assertEquals(Arrays.asList(15, 1, 5, 20, 7, 9, 10), Arrays.asList(set.toArray()));

        Assert.assertTrue(set.add(100));
        Assert.assertEquals(Arrays.asList(15, 1, 5, 20, 7, 9, 10, 100), Arrays.asList(set.toArray()));

        Assert.assertEquals(8, set.size());
    }

    @Test
    public void removeTest() {

        Assert.assertTrue(set.add(10));
        Assert.assertEquals(1, set.size());
        Assert.assertTrue(set.contains(10));

        Assert.assertTrue(set.remove(10));
        Assert.assertFalse(set.contains(10));
        Assert.assertEquals(0, set.size());

        set.add(30);

        set.add(50);
        set.add(10);

        set.add(5);
        set.add(20);
        set.add(70);
        set.add(100);
        set.add(60);
        set.add(65);
        set.add(55);
        set.add(56);
        set.add(19);

        Assert.assertEquals(Arrays.asList(30, 60, 19, 50, 5, 20, 65, 10, 70, 100, 55, 56), Arrays.asList(set.toArray()));

        set.remove(20);
        set.remove(100);

        Assert.assertEquals(Arrays.asList(30, 60, 19, 50, 5, 65, 10, 70, 55, 56), Arrays.asList(set.toArray()));

        set.remove(65);

        Assert.assertEquals(Arrays.asList(30, 60, 19, 50, 5, 10, 70, 55, 56), Arrays.asList(set.toArray()));

        set.remove(50);
        Assert.assertEquals(Arrays.asList(30, 60, 19, 5, 10, 70, 55, 56), Arrays.asList(set.toArray()));

        set.remove(30);
        Assert.assertEquals(Arrays.asList(60, 19, 5, 10, 70, 55, 56), Arrays.asList(set.toArray()));

        set.remove(5);
        set.remove(10);
        set.remove(70);
        set.remove(19);
        set.remove(55);
        set.remove(56);
        set.remove(60);

        Assert.assertEquals(0, set.size());
        Assert.assertEquals(Collections.emptyList(), Arrays.asList(set.toArray()));
    }

    @Test
    public void containsTest() {
        set.add(30);

        set.add(50);
        set.add(10);

        set.add(5);
        set.add(20);
        set.add(70);
        set.add(100);
        set.add(60);
        set.add(65);
        set.add(55);
        set.add(56);
        set.add(19);
        set.add(null);

        Assert.assertTrue(set.contains(null));
        Assert.assertFalse(set.contains(200));
        Assert.assertEquals(Arrays.asList(null, 30, 60, 19, 50, 5, 20, 65, 10, 70, 100, 55, 56), Arrays.asList(set.toArray()));

        for (Object value : set.toArray()) {

            Assert.assertTrue(set.contains((Integer) value));
        }
    }

    @Test
    public void toStringTest() {

        set.add(30);

        set.add(50);
        set.add(10);

        Assert.assertEquals("[ 30 50 10 ]", set.toString());

        set.add(5);
        set.add(20);
        set.add(70);
        set.add(100);

        Assert.assertEquals("[ 30 50 5 20 10 70 100 ]", set.toString());

        set.add(60);
        set.add(65);
        set.add(55);
        set.add(56);
        set.add(19);

        Assert.assertFalse(set.contains(200));
        Assert.assertEquals("[ 30 60 19 50 5 20 65 10 70 100 55 56 ]", set.toString());
    }

    @Test
    public void toArrayTest() {

        set.add(30);
        set.add(null);
        set.add(50);
        set.add(10);

        Assert.assertEquals(Arrays.asList(null, 30, 50, 10), Arrays.asList(set.toArray()));

        set.add(5);
        set.add(20);
        set.add(70);
        set.add(100);

        Assert.assertEquals(Arrays.asList(null, 30, 50, 5, 20, 10, 70, 100), Arrays.asList(set.toArray()));

        set.add(60);
        set.add(65);
        set.add(55);
        set.add(56);
        set.add(19);

        Assert.assertFalse(set.contains(200));
        Assert.assertEquals(Arrays.asList(null, 30, 60, 19, 50, 5, 20, 65, 10, 70, 100, 55, 56), Arrays.asList(set.toArray()));
    }
}
