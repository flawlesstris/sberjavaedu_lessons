package ru.edu;

import java.util.Objects;

public class DigitHandlerEquality {


    private int value;


    public DigitHandlerEquality(int value) {

        this.value = value;

    }


    @Override

    public boolean equals(Object o) {

        if (this == o) {

            return true;

        }

        if (o == null || getClass() != o.getClass()) {

            return false;

        }

        DigitHandlerEquality that = (DigitHandlerEquality) o;

        return value == that.value;

    }


    @Override

    public int hashCode() {

        return Objects.hash(value);

    }

}