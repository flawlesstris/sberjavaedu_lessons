Создать класс ProductRepositoryImpl, который представляет CRUD репозиторий к таблице product

Класс ProductRepositoryImpl реализует интерфейс ProductRepository 


Конструктор класса ProductRepositoryImpl:

ProductRepositoryImpl(Connection connection)

Ссылка на интерфейс ProductRepository: link

Критерии приемки

Предоставить PR, в котором присутствует реализация класса ProductRepositoryImpl.

Каждый публичный метод должен быть покрыт unit тестом.