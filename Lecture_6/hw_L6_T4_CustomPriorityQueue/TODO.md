#HW_L6_T3_CustomPriorityQueueImpl

Реализовать класс CustomPriorityQueueImpl<T>, который представляет очередь, в которой элемент с наибольшим значением попадает в начало очереди.

Класс CustomPriorityQueueImpl реализует интерфейс CustomPriorityQueue<T>
Класс CustomPriorityQueueImpl может хранить объекты любого типа
Класс CustomPriorityQueueImpl имеет фиксированный размер. Динамическое расширение не предусмотрено

##Конструкторы

CustomPriorityQueueImpl(Comparator<T> c);
CustomPriorityQueueImpl(int capacity, Comparator<T> c);

Ссылка на CustomPriorityQueue<T>: #ref

#Критерии приемки

1. Создать ветку feature/CustomPriorityQueue
2. Добавить интерфейс CustomPriorityQueue в ветку, сделать PUSH в удаленный репозиторий

3. Создать ветку feature/CustomPriorityQueueImpl от ветки feature/CustomPriorityQueue

4. Написать реализацию класса CustomPriorityQueueImpl

5. Предоставить на проверку Pull Request из ветки feature/CustomPriorityQueueImpl в ветку feature/CustomPriorityQueue

6. Каждый публичный метод класса CustomPriorityQueueImpl должен быть покрыт unit тестом

7. !!! Вносить правки в интерфейс CustomPriorityQueue<T> нельзя