#HW_L6_T2_CustomListImpl

Реализовать класс CustomListImpl<T>, который представляет двусвязный список.

Класс CustomListImpl реализует интерфейс CustomList<T>
Класс CustomListImpl может хранить объекты любого типа
Класс CustomListImpl может динамически расширяться

##Конструкторы

CustomListImpl();
CustomListImpl(Collection<T> c);

Ссылка на CustomList<T>: #ref

#Критерии приемки

1. Создать ветку feature/CustomList
2. Добавить интерфейс CustomList в ветку, сделать PUSH в удаленный репозиторий

3. Создать ветку feature/CustomListImpl от ветки feature/CustomList

4. Написать реализацию класса CustomListImpl

5. Предоставить на проверку Pull Request из ветки feature/CustomListImpl в ветку feature/CustomList

6. Каждый публичный метод класса CustomListImpl должен быть покрыт unit тестом

7. !!! Вносить правки в интерфейс CustomList<T> нельзя