package ru.edu;

import java.util.*;

public class Application {

    public static void main(String[] args) {

//        collectionExample();
//        severalCollectionsExample();
//        listExample();
//        queueExample();
//        dequeExample();

//        arrayListExample();
//        linkedListExample();
//        arrayDequeExample();
        priorityQueueExample();
    }

    private static void priorityQueueExample() {

//        Queue<String> queue = new PriorityQueue<>();
//
//        queue.add("Anton");
//        queue.add("Vasiliy");
//        queue.add("Maria");
//
//        System.out.println("queue.element(): " + queue.element());

        Queue<Integer> queue = new PriorityQueue<>();

        queue.add(1);
        queue.add(100);
        queue.add(50);

        System.out.println("queue.element(): " + queue.element());
        queue.remove();

        System.out.println("queue.element(): " + queue.element());
        queue.remove();

        System.out.println("queue.element(): " + queue.element());
        queue.remove();
    }

    private static void arrayDequeExample() {

        ArrayDeque<String> deque = new ArrayDeque<>();

        deque.addFirst("Anton");
        deque.addLast("Denis");
        deque.addFirst("Vasya");

        while (!deque.isEmpty()) {

            String value = deque.peekLast();
            System.out.println("Elem: " + value);
            deque.removeLast();
        }
    }

    private static void linkedListExample() {

        LinkedList<String> list = new LinkedList<>();

        list.add("Denis");
        list.add("Anton");
        list.add("Denis");
        list.add("Dmitriy");
        list.add("Vasilisa");

        System.out.println(list);

        list.addFirst("FirstElement");
        System.out.println(list);

        list.addLast("LastElement");
        System.out.println(list);

        System.out.println("list.peekFirst(): " + list.peekFirst());
        System.out.println("list.peekLast(): " + list.peekLast());
    }

    private static void arrayListExample() {

        List<String> list = new ArrayList<>(5);

        list.add("Denis");
        list.add("Anton");
        list.add("Denis");
        list.add("Dmitriy");
        list.add("Vasilisa");

        System.out.println(list);

        list.add("Denis2");
        list.add("Anton2");
        list.add("Denis2");
        list.add("Dmitriy2");
        list.add("Vasilisa2");

        System.out.println(list);

//        System.out.println("list.get(3): " + list.get(3));
//
//        for (int i = 0; i < list.size(); ++i) {
//            System.out.println("Elem: " + list.get(i));
//        }
    }

    private static void dequeExample() {

        Deque<String> myCollection = new LinkedList<>();

        myCollection.addLast("Denis");
        myCollection.addLast("Anton");
        myCollection.addFirst("Denis");
        myCollection.addFirst("Dmitriy");
        myCollection.addLast("Vasilisa");

//        String value;
//        while ((value = myCollection.pollFirst()) != null) {
//
//            System.out.println("Elem: " + value);
//        }

        String value;
        while ((value = myCollection.pollLast()) != null) {

            System.out.println("Elem: " + value);
        }
    }

    private static void queueExample() {

        Queue<String> myCollection = new LinkedList<>();

        myCollection.add("Denis");
        myCollection.add("Anton");
        myCollection.add("Denis");
        myCollection.add("Dmitriy");
        myCollection.add("Vasilisa");

        System.out.println("myCollection.element: " + myCollection.element());
        System.out.println("myCollection.element: " + myCollection.peek());

        System.out.println("myCollection.remove(): " + myCollection.remove());

        System.out.println("myCollection.element: " + myCollection.element());
        System.out.println("myCollection.element: " + myCollection.peek());

        String value;
        while ((value = myCollection.poll()) != null) {

            System.out.println("Elem: " + value);
        }
    }

    private static void listExample() {

        List<String> myCollection = new ArrayList<>();

        myCollection.add("Denis");
        myCollection.add("Anton");
        myCollection.add("Denis");
        myCollection.add("Dmitriy");
        myCollection.add("Vasilisa");

        System.out.println(myCollection);

        System.out.println("myCollection.get(4): " + myCollection.get(4));
        System.out.println("myCollection.get(1): " + myCollection.get(1));

        myCollection.set(1, "Anna");
        System.out.println(myCollection);

//        myCollection.add(1, "Inna");
//        System.out.println(myCollection);

//        myCollection.remove(1);
//        System.out.println(myCollection);

//        myCollection.remove("Vasilisa");
//        System.out.println(myCollection);
    }

    private static void severalCollectionsExample() {

//        Collection<String> myCollection = new TreeSet<>();
//
//        myCollection.add("Denis");
//        myCollection.add("Anton");
//        myCollection.add("Denis");
//        myCollection.add("Dmitriy");
//        myCollection.add("Vasilisa");
//
//        System.out.println("Collection: " + myCollection);
//        System.out.println("Size: " + myCollection.size());
    }

    private static void collectionExample() {

        Collection<String> myCollection = new ArrayList<>();

//        System.out.println("Collection: " + myCollection);
//
//        System.out.println("Size: " + myCollection.size());
//        System.out.println("IsEmpty: " + myCollection.isEmpty());
//
//        myCollection.add("Denis");
//        myCollection.add("Anton");
//        myCollection.add("Denis");
//        myCollection.add("Dmitriy");
//        myCollection.add("Vasilisa");
//
//        System.out.println("Collection: " + myCollection);

//        System.out.println("Size: " + myCollection.size());
//        System.out.println("IsEmpty: " + myCollection.isEmpty());
//
//        System.out.println("contains('Denis'): " + myCollection.contains("Denis"));
//        System.out.println("contains('Vasya'): " + myCollection.contains("Vasya"));

//        myCollection.remove("Denis");
//        System.out.println("Collection: " + myCollection);
//        System.out.println("contains('Denis'): " + myCollection.contains("Denis"));

//        myCollection.removeIf(str -> {
//            return str.startsWith("D");
//        });
//        System.out.println("Collection: " + myCollection);
//        System.out.println("contains('Denis'): " + myCollection.contains("Denis"));

//        Collection<String> anotherCollection = new LinkedList<>();
//
//        anotherCollection.add("Vasilisa");
//
//        boolean result = myCollection.retainAll(anotherCollection);
//        System.out.println("myCollection.retainAll: " + result);
//        System.out.println("Collection: " + myCollection);
    }
}
