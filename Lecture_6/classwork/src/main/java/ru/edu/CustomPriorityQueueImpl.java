package ru.edu;

import java.util.Comparator;
import java.util.NoSuchElementException;

public class CustomPriorityQueueImpl<E> {

    private Comparator<E> comparator;
    private Object[] data;

    public int getSize() {
        return size;
    }

    private int size;

    public CustomPriorityQueueImpl(Comparator<E> comparator) {
        this.comparator = comparator;
        this.data = new Object[10];
        this.size = 0;
    }


    public void add(E item) {

        if (size == data.length) {
            throw new IllegalStateException("Queue is full. Sorry");
        }

        int posForInsert = 0;
        while (posForInsert < size && comparator.compare((E) data[posForInsert], item) > 0) {
            ++posForInsert;
        }

        for (int j = size - 1; j >= posForInsert; --j) {
            data[j + 1] = data[j];
        }

        data[posForInsert] = item;
        ++size;
    }

    @Override
    public String toString() {

        StringBuilder cb = new StringBuilder();
        cb.append("[");

        for (int i = 0; i < size; ++i) {

            cb.append(" " + data[i]);
        }

        cb.append("]");
        return cb.toString();
    }

    public boolean isEmpty() {

        return size != 0;
    }

    public E remove() {
        if (size == 0) {
            throw new NoSuchElementException("Queue is empty");
        }

        E value =  (E) data[size - 1];
        --size;

        return value;
    }

    public E poll() {

        try {
            return remove();
        } catch (NoSuchElementException ex) {
            return null;
        }
    }
}
