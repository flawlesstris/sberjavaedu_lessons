package ru.edu;

public class CustomArrayListImpl<T> {

    private Object[] data;
    private int size;

    public CustomArrayListImpl(int initialCapacity) {
        this.data = new Object[initialCapacity];
        this.size = 0;
    }

    public int getSize() {
        return this.size;
    }

    public boolean add(T item) {

        ensureCapacity(1);

        data[size] = item;
        ++size;
        return true;
    }

    public boolean addAll(T[] items) {

        ensureCapacity(items.length);

        for (T item : items) {
            add(item);
        }
        return items.length != 0;
    }

    public void ensureCapacity(int newItemsCount) {

        int totalItemsCount = size + newItemsCount;
        if (totalItemsCount <= data.length) {
            return;
        }

        int newCapacity = 2 * data.length;
        if (totalItemsCount > newCapacity) {
            newCapacity = totalItemsCount;
        }

        Object[] newData = new Object[newCapacity];
        for (int i = 0; i < size; ++i) {
            newData[i] = data[i];
        }

        this.data = newData;
    }

    public String toString() {

        StringBuilder cb = new StringBuilder();

        cb.append("[ ");

        for (int i = 0; i < size; ++i) {
            cb.append(data[i]);
            cb.append(" ");
        }

        cb.append("]");
        return cb.toString();
    }

    public int getCapacity() {

        return data.length;
    }
}
