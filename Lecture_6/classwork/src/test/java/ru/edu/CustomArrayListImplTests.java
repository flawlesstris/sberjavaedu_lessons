package ru.edu;

import org.junit.Assert;
import org.junit.Test;

public class CustomArrayListImplTests {

    @Test
    public void arrayListTests() {

        CustomArrayListImpl<String> arrayList = new CustomArrayListImpl<>(1);

        System.out.println(arrayList);
        Assert.assertEquals(0, arrayList.getSize());

        arrayList.add("Anton");
        System.out.println(arrayList);
        Assert.assertEquals(1, arrayList.getSize());

        for (int i = 0; i < 100; ++i) {
            arrayList.add("Test" + i);
        }

        System.out.println(arrayList);
        Assert.assertEquals(101, arrayList.getSize());
    }

    @Test
    public void addAllTest() {

        String[] collection = new String[100];
        for (int i = 0; i < 100; ++i) {
            collection[i] = "Test" + i;
        }

        CustomArrayListImpl<String> arrayList = new CustomArrayListImpl<>(1);
        arrayList.addAll(collection);

        Assert.assertEquals(100, arrayList.getSize());
        Assert.assertEquals(100, arrayList.getCapacity());
    }
}
