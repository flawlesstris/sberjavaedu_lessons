package ru.edu;

import org.junit.Assert;
import org.junit.Test;

public class CustomArrayDequeImplTests {

    @Test
    public void arrayDequeTest() {

        CustomArrayDequeImpl<String> deque = new CustomArrayDequeImpl<>();

        Assert.assertEquals(0, deque.size());
        System.out.println(deque.toString());

        deque.addLast("1");
        Assert.assertEquals(1, deque.size());
        System.out.println(deque.toString());

        for (int i = 2; i <= 10; ++i) {
            deque.addLast("" + i);
        }
        Assert.assertEquals(10, deque.size());
        System.out.println(deque.toString());

        System.out.println("deque.offerLast: " + deque.offerLast("1000"));
    }

    @Test
    public void removeTest() {

        CustomArrayDequeImpl<String> deque = new CustomArrayDequeImpl<>();

        for (int i = 1; i <= 10; ++i) {
            deque.addLast("" + i);
        }
        Assert.assertEquals(10, deque.size());
        System.out.println(deque.toString());


        while (!deque.isEmpty()) {

            System.out.println("Elem: " + deque.removeLast());
        }

        Assert.assertEquals(0, deque.size());
        System.out.println(deque);
    }
}
